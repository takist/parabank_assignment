﻿Feature: Registration

A short summary of the feature

@smoke
Scenario: User can access registrion page
	Given Go to the registration page
	Then registration page is fully loaded and visible


Scenario: User can register a simple account with all fields
	Given Go to the registration page
	When User give tt as firstName to register
	And User give tt as lastName to register
	And User give tt as address.street to register
	And User give tt as address.city to register
	And User give tt as address.state to register
	And User give 11 as address.zipCode to register
	And User give 00 as phoneNumber to register
	And User give 00 as ssn to register
	And User give tt as username to register
	And User give tt as password to register
	And User give tt as confirm password for register
	And User press register button
	Then Register succesfull