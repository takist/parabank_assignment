﻿Feature: Login

A short summary of the feature

@smoke
Scenario: User can access landing page
	Given Go to the start login page
	Then Landing page is fully loaded and visible

@smoke
Scenario: User can access login form
	Given Go to the start login page
	Then Login form and fields are visible, accesible


Scenario: User can log in with valid credentials
	Given Go to the start login page
	And User with ff as username and ff as pass exists
	When User give ff as username for login
	And User give ff as password for login
	And User press login button
	Then Login succesfull

Scenario: User can not log in with invalid credentials
	Given Go to the start login page
	And User with gy as username and gy as pass not exists
	When User give gy as username for login
	And User give gy as password for login
	And User press login button
	Then Login unsuccesfull