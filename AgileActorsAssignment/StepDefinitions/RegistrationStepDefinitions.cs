﻿using Microsoft.Playwright;
using BoDi;

namespace AgileActorsAssignment.StepDefinitions
{
    [Binding]
    public class RegistrationStepDefinitions
    {
        private readonly ParaBankPage _page;
        private IResponse _response;

        public RegistrationStepDefinitions(IObjectContainer container)
        {
            _page = container.Resolve<ParaBankPage>();
        }

        [Given(@"Go to the registration page")]
        public async Task GivenLoginPageOpens()
        {
            _response = await _page.gotoPage("/parabank/register.htm");
        }

        [When(@"User give (.*) as (.*) to register")]
        public async Task WhenUserGiveUserNameForRegister(string userInput, string field)
        {
            await _page.fillUserRegisterField(userInput, field);
        }

        [When(@"User give (.*) as confirm password for register")]
        public async Task WhenUserGiveConfirmPassForRegister(string pass)
        {
            await _page.fillPassConfirmRegister(pass);
        }

        [When(@"User press register button")]
        public async Task WhenUserPressForRegister()
        {
            await _page.pressRegisterButton();
        }

        [Then(@"registration page is fully loaded and visible")]
        public async Task RegistrationIsFullyLoaded()
        {
            var sign = await _page.LocateIfExistsInPage("Signing up is easy");
            var usr = await _page.LocateIfExistsInPage("Username:");
            var pass = await _page.LocateIfExistsInPage("Password:");
            var confirm = await _page.LocateIfExistsInPage("Confirm:");
            var register = await _page.LocateIfExistsInPage("Register");
            var form = await _page.LocateRegisterForm();
            _response.Ok.Should().BeTrue();
            sign.Should().BeTrue();
            usr.Should().BeTrue();
            pass.Should().BeTrue();
            confirm.Should().BeTrue();
            register.Should().BeTrue();
            form.Should().BeTrue();
        }

        [Then(@"Register succesfull")]
        public async Task ThenRegisterSuccesfull()
        {
            //var errorTitle = await _page.errorTitleText();
            //var errorText = await _page.errorText();
            //errorTitle.Should().Contain("Error");
            //errorText.Should().Contain("The username and password could not be verified");
        }
    }
}
