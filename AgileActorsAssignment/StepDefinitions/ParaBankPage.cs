﻿using Microsoft.Playwright;

namespace AgileActorsAssignment.StepDefinitions
{
    public class ParaBankPage
    {
        private readonly IPage _pageObject;
        public readonly string ErrorLoginMessage = "The username and password could not be verified";
        public readonly string ErrorRegisterFirst= "First name is required";
        public readonly string ErrorRegisterLast = "Last name is required";
        public readonly string ErrorRegisterAddr = "Address is required";
        public readonly string ErrorRegisterCity = "City is required";
        public readonly string ErrorRegisterState = "State is required";
        public readonly string ErrorRegisterZip = "Zip Code is required";
        public readonly string ErrorRegisterSsn = "Social Security Number is required";
        public readonly string ErrorRegisterUser = "Username is required";
        public readonly string ErrorRegisterExistsUser = "";
        public readonly string ErrorRegisterPass = "Password is required";
        public readonly string ErrorRegisterConfirmPass = "Password confirmation is required";
        public readonly string RegisterSuccess = "Your account was created successfully. You are now logged in";

        public ParaBankPage(IPage givenPage)
        {
            _pageObject = givenPage;
        }

        public async Task<IResponse> gotoPage(string targetPage)
        {
            return await _pageObject.GotoAsync(targetPage);
        }

        public async Task fillUserNameLogin(string givenUserName)
        {
            var locatorUsername = _pageObject.Locator("[id=loginPanel] [name=login] [class=login] [name=username]");
            await locatorUsername.FillAsync(givenUserName);
        }

        public async Task fillPasswordLogin(string givenPass)
        {
            var locatorPassword = _pageObject.Locator("[id=loginPanel] [name=login] [class=login] [name=password]");
            await locatorPassword.FillAsync(givenPass);
        }

        public async Task pressLoginButton()
        {
            var locatorSubmitButton = _pageObject.Locator("[id=loginPanel] [name=login] [class=login] [type=submit]");
            await locatorSubmitButton.ClickAsync();
            await _pageObject.WaitForLoadStateAsync();
        }

        public async Task<string> errorOnLoginTitleText()
        {
            return await _pageObject.Locator("[id=rightPanel] [class=title]").TextContentAsync();
        }

        public async Task<string> errorOnLoginText()
        {
            return await _pageObject.Locator("[id=rightPanel] [class=error]").TextContentAsync();
        }

        public async Task<bool> LocateIfExistsInPage(string givenString)
        {
            var allContent = await _pageObject.ContentAsync();
            return allContent.Contains(givenString);
        }

        public async Task<bool> LocateRegisterForm()
        {
            return await _pageObject.Locator("[id=customerForm]").IsVisibleAsync();
        }

        public async Task fillUserRegisterField(string userInput, string dotSelector)
        {
            var locatorUserFillRegister = _pageObject.Locator($"id=customer.{dotSelector}");
            await locatorUserFillRegister.FillAsync(userInput);
        }

        public async Task fillPassConfirmRegister(string givenPass)
        {
            var locatorPassConfirmFillRegister = _pageObject.Locator("id=repeatedPassword");
            await locatorPassConfirmFillRegister.FillAsync(givenPass);
        }

        public async Task pressRegisterButton()
        {
            var locatorRegisterButton = _pageObject.Locator("[id=customerForm] [class=form2] [value=Register]");
            await locatorRegisterButton.ClickAsync();
            await _pageObject.WaitForLoadStateAsync();
        }

        public async Task<bool> LogOutExists()
        {
            return await _pageObject.Locator("text=Log Out").IsVisibleAsync();
        }

        public async Task LogOut()
        {
            await _pageObject.Locator("text=Log Out").ClickAsync();
        }

        public async Task<bool> userExists(string givenUser, string givenPass)
        {
            var initUrl = _pageObject.Url;

            var flagReturned = false;
            await _pageObject.GotoAsync("/parabank/index.htm");

            await this.fillUserNameLogin(givenUser);
            await this.fillPasswordLogin(givenPass);
            await this.pressLoginButton();
            var returnedError = await this.LogOutExists();
            if (returnedError)
            {
                flagReturned = true;
                await this.LogOut();
            }

            await _pageObject.GotoAsync(initUrl);

            return flagReturned;
        }

        public async Task<bool> RegisterNewUser(
            string username, string pass, string firstname, 
            string lastName, string street, string city,
            string state, string zipCode, string phoneNumber,
            string ssn)
        {
            var initUrl = _pageObject.Url;

            var flagReturned = false;
            await _pageObject.GotoAsync("/parabank/register.htm");
            
            await this.fillUserRegisterField(firstname, "firstName");
            await this.fillUserRegisterField(lastName, "lastName");
            await this.fillUserRegisterField(street, "address.street");
            await this.fillUserRegisterField(city, "address.city");
            await this.fillUserRegisterField(state, "address.state");
            await this.fillUserRegisterField(zipCode, "address.zipCode");
            await this.fillUserRegisterField(phoneNumber, "phoneNumber");
            await this.fillUserRegisterField(ssn, "ssn");
            await this.fillUserRegisterField(username, "username");
            await this.fillUserRegisterField(pass, "password");
            await this.fillPassConfirmRegister(pass);
            await this.pressRegisterButton();

            var returnedStatus = await this.LocateIfExistsInPage(RegisterSuccess);
            if (returnedStatus)
            {
                flagReturned = true;
                await this.LogOut();
            }

            await _pageObject.GotoAsync(initUrl);
            return flagReturned;
        }
    }
}
