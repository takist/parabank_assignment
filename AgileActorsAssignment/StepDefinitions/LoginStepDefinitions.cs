using Microsoft.Playwright;
using BoDi;

namespace AgileActorsAssignment.StepDefinitions
{
    [Binding]
    public class LoginStepDefinitions
    {
        private readonly ParaBankPage _page;
        private IResponse _response;

        public LoginStepDefinitions(IObjectContainer container)
        {
            _page =  container.Resolve<ParaBankPage>();
        }

        [Given(@"Go to the start login page")]
        public async Task GivenLoginPageOpens()
        {
            _response = await _page.gotoPage("/parabank/index.htm");
        }

        [Given(@"User with (.*) as username and (.*) as pass exists")]
        public async Task GivenUserExistOrCreate(string username, string pass)
        {
            var userExist = await _page.userExists(username, pass);
            if (!userExist)
            {
                await _page.RegisterNewUser(username, pass, "example", "example", 
                    "example", "example", "example", "example", "example", "example");
            }
        }

        [Given(@"User with (.*) as username and (.*) as pass not exists")]
        public async Task GivenUserNotExist(string username, string pass)
        {
            var userExist = await _page.userExists(username, pass);
            userExist.Should().BeFalse();
        }

        [When(@"User give (.*) as username for login")]
        public async Task WhenUserGiveUserNameForLogin(string username)
        {
            await _page.fillUserNameLogin(username);
        }

        [When(@"User give (.*) as password for login")]
        public async Task WhenUserGivePasswordForLogin(string pass)
        {
            await _page.fillPasswordLogin(pass);
        }

        [When(@"User press login button")]
        public async Task WhenUserPressLoginButton()
        {
            await _page.pressLoginButton();
        }

        [Then(@"Login succesfull")]
        public async Task ThenLoginSuccesfull()
        {
            var logout = await _page.LocateIfExistsInPage("Log Out");
            var accountsOverview = await _page.LocateIfExistsInPage("Accounts Overview");
            var availableAmount = await _page.LocateIfExistsInPage("Available Amount");
            logout.Should().BeTrue();
            accountsOverview.Should().BeTrue();
            availableAmount.Should().BeTrue();
        }

        [Then(@"Landing page is fully loaded and visible")]
        public async Task LandingPageIsLoaded()
        {
            var paraBank = await _page.LocateIfExistsInPage("ParaBank");
            var aboutUs = await _page.LocateIfExistsInPage("About Us");
            _response.Url.Should().Contain("index");
            _response.Ok.Should().BeTrue();
            paraBank.Should().BeTrue();
            aboutUs.Should().BeTrue();
        }

        [Then(@"Login form and fields are visible, accesible")]
        public async Task Loginformandfields()
        {
            var logIn = await _page.LocateIfExistsInPage("Log In");
            var username = await _page.LocateIfExistsInPage("Username");
            var password = await _page.LocateIfExistsInPage("Password");
            logIn.Should().BeTrue();
            username.Should().BeTrue();
            password.Should().BeTrue();
            // should also add checks for that ae accesible, didndt had time
        }

        [Then(@"Login unsuccesfull")]
        public async Task ThenLoginUnSuccesfull()
        {
            var errorTitle = await _page.errorOnLoginTitleText();
            var errorText = await _page.errorOnLoginText();
            errorTitle.Should().Contain("Error");
            errorText.Should().Contain(_page.ErrorLoginMessage);
        }
    }
}
