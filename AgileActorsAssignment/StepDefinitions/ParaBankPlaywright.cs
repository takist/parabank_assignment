﻿using BoDi;
using Microsoft.Playwright;

namespace AgileActorsAssignment.StepDefinitions
{
    [Binding]
    public class ParaBankPlaywright
    {
        [BeforeScenario]
        public async Task BeforeScenario(IObjectContainer container)
        {
            var playwright = await Playwright.CreateAsync();

            var browser = await playwright.Chromium.LaunchAsync(new BrowserTypeLaunchOptions
            {
                Headless = false,
                Timeout = 8 * 1000,
                //SlowMo = 3 * 1000
            });

            var browserContext = await browser.NewContextAsync(new BrowserNewContextOptions
            {
                BaseURL = "https://parabank.parasoft.com/"
            });

            var page = await browserContext.NewPageAsync();

            var pageInteractionable = new ParaBankPage(page);

            container.RegisterInstanceAs(playwright);
            container.RegisterInstanceAs(browser);
            container.RegisterInstanceAs(pageInteractionable);
        }

        [AfterScenario]
        public async Task AfterScenario(IObjectContainer container)
        {
            var browser = container.Resolve<IBrowser>();
            await browser.CloseAsync();
            var playwright = container.Resolve<IPlaywright>();
            playwright.Dispose();
        }
    }
}
